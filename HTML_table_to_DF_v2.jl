
using Cascadia, DataFrames, Gumbo, HTTP
## FÜGGVÉNYEK -------------------------
#
# HTML_TABLAK(url)
# Egy weboldalról összeszedi a táblázatokat
function HTML_TABLAK(url)
    if url isa String == false
        println("Az url nem String típusú!")
    end
    # htnl táblák kiválasztása html fileból
    #url = "https://static.valasztas.hu/dyn/onk14/szavossz/hu/M01/T001/szk001.html"
    res = HTTP.get(url)
    body = String(res.body)
    html = parsehtml(body)
    return eachmatch(sel"table", html.root)
end

# HTML_TO_DF(html_table)
# egy HTML táblából dataframe-et készít
# soronként
function HTML_TO_DF(html_table)
    # sorok
    sorok = eachmatch(sel"tr", html_table)

    tab_adat = []
    tab_fejlec = []
    for sor in sorok
        # adattartalom
        if isempty(eachmatch(sel"th", sor)) == true
            cellak = eachmatch(sel"td, th", sor)
            sor_t_1 = []
            for cl in cellak
                push!(sor_t_1, nodeText(cl))
            end
            push!(tab_adat, sor_t_1)
        else
            cellak = eachmatch(sel"td, th", sor)
            sor_t_1 = []
            for cl in cellak
                push!(sor_t_1, replace(nodeText(cl), r"\s" => "_"))
            end
            push!(tab_fejlec, sor_t_1)
        end
    end

    tab_ki = Array{String}(undef, length(tab_adat), length(tab_adat[1]))
    for sr in collect(1:length(tab_adat))
        tab_ki[sr, :] = tab_adat[sr]
    end

    df = DataFrame(tab_ki)
    if isempty(tab_fejlec) == false && length(tab_fejlec[1]) == size(df, 2)
        names!(df, Symbol.(tab_fejlec[1]))
    end

    return df

end


# Önkormányzati választás eredményei -----------------------------
# megyék alapadatai
# megyék beolvasása adatkészletbe
url_megye = "https://static.valasztas.hu/dyn/onk14/vertaj/hu/v2m.html"
megye_df = map(x -> HTML_TO_DF(x), HTML_TABLAK(url_megye))

megye_df = megye_df[1]
megye_df[!,:sorszam] = map(x -> string(x)[2:3], collect(1:nrow(megye_df)) .+ 100)
megye_df[!, 2] = map(x->parse(Int64, replace(x, r"\s"=>"")), megye_df[!,2])
megye_df[!, 3] = map(x->parse(Int64, replace(x, r"\s"=>"")), megye_df[!,3])

# Települések ----------------------
# megye települései
# településenként a szavazókörök száma
# először 20 elemű listába
function ORSZAG_TELEP_14()
    orsz_t_lista = []
    for tl in megye_df[!,:sorszam]
        url_tl = "https://static.valasztas.hu/dyn/onk14/vertaj/hu/M" * tl * "/v2t.html"
        megye_t_lista = map(x -> HTML_TO_DF(x), HTML_TABLAK(url_tl))[1]
        megye_t_lista[!, :megye] = tl
        # href településazonosítók
        m_tbl = HTML_TABLAK(url_tl)
        sorok = eachmatch(sel"tr", m_tbl[1])
        thref = Array{String}(undef, 0)
        for sor in 3:length(sorok)
            push!(thref, SubString(collect(values(attrs(eachmatch(sel"a", sorok[sor][1])[1])))[1],2,4))
        end
        megye_t_lista[!, :t_sorszam] = thref
        push!(orsz_t_lista, megye_t_lista)
    end

    # ország összes települése egy adatkészletbe
    orsz = orsz_t_lista[1]
    for ml in collect(2:length(orsz_t_lista))
        append!(orsz, orsz_t_lista[ml])
    end

    return orsz
end

# Budapesti eredmények --------------------------------
# budapesti sorainak szűrése az országos adatokból
function BP_SZK()
    df_bp = orsz[orsz.megye .== "01", : ]
    # orsz[11,:] a 11. sor az adatokból
    # megye orsz[11,4]
    # Település orsz[11,5]

    # budapesti szavazókörök adatainak tömbje
    bp_df1 = []
    # a 3 eredménylista a választási típusokhoz
    sz_tipus = ["főpolg","kerületi polgármester","önkorm. képviselőjelölt"]

    # soronként a budapesti adatokon
    for k in collect(1:nrow(df_bp))

        #megye ID
        MGY = df_bp[k,:megye]
        TSOR = df_bp[k,:t_sorszam]

        # szavazókörök száma
        szk_ht = HTML_TABLAK("https://static.valasztas.hu/dyn/onk14/vertaj/hu/M"*MGY*"/T"*TSOR*"/v21.html")
        if length(szk_ht) == 2
            szkn = HTML_TO_DF(szk_ht[2])[:,2]
        else
            szkn = HTML_TO_DF(szk_ht[1])[:,2]
        end


        # ciklus a szavazókörökre
        for szi in szkn
            szlink = "https://static.valasztas.hu/dyn/onk14/szavossz/hu/M" * df_bp[k,4] *
            "/T" * df_bp[k,5] * "/szk" * szi *".html"

            # Bp szavazókörök
            # 6: főpolg
            # 10: kerületi polgármester
            # 14: önkorm. képviselőjelölt
            szk_tbl = map(x -> HTML_TO_DF(x), HTML_TABLAK(szlink)[[6,10,14]])

            # azonosító adatok hozzáfűzése a szavazókör adataihoz
            for j in [1 2 3]
                szk_tbl[j][!, :megye] = df_bp[k,4]
                szk_tbl[j][!, :telep_n] = df_bp[k,5]
                szk_tbl[j][!, :szavazokor] = szi
                szk_tbl[j][!, :szav_tipus] = sz_tipus[j]
            end

            szk_df0 = szk_tbl[1]
            append!(szk_df0, szk_tbl[2])
            append!(szk_df0, szk_tbl[3])

            push!(bp_df1, szk_df0)
        end
    end

    # Budapest kerületeinek összes szavazóköri adata egy dataframe-be
    ker_szk = bp_df1[1]
    for ml in collect(2:length(bp_df1))
        append!(ker_szk, bp_df1[ml])
    end

    return ker_szk
end

orsz = ORSZAG_TELEP_14()
ker_szk = BP_SZK()

# átalakítás
ker_szk[!, :Sorszám] = convert(Array{String}, ker_szk[!, :Sorszám])
ker_szk[!, :Sorszám] = map(x->parse(Int64, strip(x)), ker_szk[!, :Sorszám])
# ide jön -------------------
#ker_szk[!, :Kapott_érvényes_szavazat] = map(x->parse(Int64, replace(strip(x), "-"=>"0")),
#    ker_szk[!, :Kapott_érvényes_szavazat])


# Megyei jogú városok
mjv = HTML_TO_DF(HTML_TABLAK("https://static.valasztas.hu/dyn/onk14/szavossz/hu/kiemelt.html")[3])

mjv_var=Array{Bool}(undef, 0)
for i in 1:nrow(orsz)
    push!(mjv_var, (orsz[i,1] ∈ mjv[!,2] && orsz[i,:megye] != "01"))
end
orsz[!, :MJV] = mjv_var


#------------------
# MJV adatok
# tábla: 6, 10

df_mjv = orsz[orsz.MJV .== true, : ]

# a 3 eredménylista a választási típusokhoz
sz_tipus = ["polgármester","önkorm. képviselőjelölt"]

# mjv szavazókörök adatainak tömbje
mjv_df1 = []

# soronként a budapesti adatokon
for k in collect(1:nrow(df_mjv))

    #megye ID
    MGY = df_mjv[k,:megye]
    TSOR = df_mjv[k,:t_sorszam]

    # szavazókörök száma
    szk_ht = HTML_TABLAK("https://static.valasztas.hu/dyn/onk14/vertaj/hu/M"*MGY*"/T"*TSOR*"/v21.html")
    if length(szk_ht) == 2
        szkn = HTML_TO_DF(szk_ht[2])[!,2]
    else
        szkn = HTML_TO_DF(szk_ht[1])[!,2]
    end

    # ciklus a szavazókörökre
    #szi=szkn[1]
    for szi in szkn
        szlink = "https://static.valasztas.hu/dyn/onk14/szavossz/hu/M" * df_mjv[k,4] *
        "/T" * df_mjv[k,5] * "/szk" * szi *".html"

        # mjv szavazókörök
        # 6: polg
        # 10: önkorm. képviselőjelölt
        szk_tbl = map(x -> HTML_TO_DF(x), HTML_TABLAK(szlink)[[6,10]])

        # azonosító adatok hozzáfűzése a szavazókör adataihoz
        for j in [1, 2]
            szk_tbl[j][!, :megye] .= df_mjv[k,4]
            szk_tbl[j][!, :telep_n] .= df_mjv[k,5]
            szk_tbl[j][!, :szavazokor] .= szi
            szk_tbl[j][!, :szav_tipus] .= sz_tipus[j]
        end

        szk_df0 = szk_tbl[1]
        append!(szk_df0, szk_tbl[2])
        push!(mjv_df1, szk_df0)
    end
end

# MJV összes szavazóköri adata egy dataframe-be
mjv_szk = mjv_df1[1]
for ml in collect(2:length(mjv_df1))
    append!(mjv_szk, mjv_df1[ml])
end

mjv_szk[!, :Sorszám] = map(x->parse(Int64, strip(x)), mjv_szk[!, :Sorszám])
#mjv_szk[!, :Kapott_érvényes_szavazat] = map(x->parse(Int64, replace(strip(x), "-"=>"0")),
#    mjv_szk[!, :Kapott_érvényes_szavazat])

# bp és mjv egybe
varos_szk = append!(ker_szk, mjv_szk)

szv = Array{Union{Missing, Int64}}(undef, 0)
for szi in varos_szk.Kapott_érvényes_szavazat
    if strip(szi) == "-"
        push!(szv, missing)
    else
        push!(szv, parse(Int64, strip(szi)))
    end
end
varos_szk.Kapott_érvényes_szavazat = szv

using CSV
CSV.write("varos_szk.csv",  varos_szk, writeheader=true, quotechar = '"',
quotestrings = true)
